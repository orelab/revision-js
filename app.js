/*
    fonction fléchée
    JSON
    manipulations en tous genres
*/


janvier = [1, 2, 3, 4, '...', 31];
fevrier = [1, 2, 3, 4, '...', 28];
mars = [1, 2, 3, 4, '...', 31]
calendrier = [janvier, fevrier, mars, []];

// console.log( [1, 2, 3, 4, '...', 31][3] );
// console.log( janvier[3] );
// console.log( [ calendrier[0][3] ,2,'A'] );


// JSON

personne = {
    'prenom': 'Aurélien',
    'nom': 'Chirot',
    'age': 39,
    'taille': 175,
    'XP': {
        'ALD': 2, 
        'FW':10 , 
        'IL':8,
        'SPL':4
    },

    'marcher': function (xxx, zzz) { return yyy },
    'courir': () => yyy,
    'manger': xxx => yyy,
    'grandir': cm => 'Je suis plus grand de ' + cm + 'cm !'
};

console.log( personne['prenom'] );
console.log( personne.prenom );
console.log( personne['XP']['SPL'] );
console.log( personne.XP.SPL );

console.log(personne.grandir(4) );


// Fonctions

function jeFaisQuelqueChose(param) {
    return param + 'sdfsf';
}


let jeFaisQuelqueChose2 = function (param) {
    return param + 'sdfsf';
}

jeFaisQuelqueChose2('coucou');


const demo1 = xxx => xxx + 'A';
const demo2 = xxx => xxx + 'B';
const demo3 = (xxx) => { return xxx + 'C'; }
const demo4 = function (xxx) { return xxx + 'D'; }
function demo5(xxx) { return xxx + 'E'; }



// console.log(demo3('Manu'));
// console.log(demo3());

