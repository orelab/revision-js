
export default class Zoo {

    nom = '';

    listeAnimaux = [];

    constructor(nom) {
        this.nom = nom;
    }

    add(animal) {
        this.listeAnimaux.push(animal);
    }

    afficher() {
        let html = document.querySelector('#zoo');

        // Ajouter le titre
        var h1 = document.createElement("h1");                 // Create a <li> node
        var nomZoo = document.createTextNode(this.nom);         // Create a text node
        h1.appendChild(nomZoo);                              // Append the text to <li>
        html.appendChild(h1);

        // Afficher chacun des animaux
        this.listeAnimaux.forEach(a =>  a.afficher(html) );
    }
}