
export default class Animal {

    nom;
    poids;
    dateNaissance; // date
    dateMort; // date
    regimeAlimentaire; // string
    estomac = 0;
    energie = 10;
    latitude;
    longitude;
    carnetDeNourrissage = [];
    estFemelle = true;

    constructor(nom, poids, estFemelle){
        this.nom = nom;
        this.poids = poids;
        this.estFemelle = estFemelle;
    }

    manger(kiloNourriture){
        this.carnetDeNourrissage.push({date:new Date(),qt:kiloNourriture});

        this.estomac += kiloNourriture / 2;
        this.energie += kiloNourriture / 2;
    }

    faireCaca(kiloCaca){
        this.estomac -= kiloCaca;
    }

    courir(coordoonnes){}

    afficher(balise){
        var div = document.createElement("div");                 // Create a <li> node
        var nomAnimal = document.createTextNode(this.nom);         // Create a text node
        div.appendChild(nomAnimal);                              // Append the text to <li>
        balise.appendChild(div);
    }

}