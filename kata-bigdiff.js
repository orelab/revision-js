var tab1 = [
    ["A",   "B",   "C"],
    [ 1,     2,     3],
    [true, false, false]
];

/* devient :
var tab2 = [
    [true,  1, "A"],
    [false, 2, "B"],
    [false, 3, "C"]
];
*/


/*
Pour chaque colonne de tab1
Je stocke chacune des valeurs dans une nouvelle liste (en commençant par la dernière valeur de la colonne)
Puis quand cette liste est faite, je l'ajoute au tableau de résultat final
*/

function convertir_tableau(original) {
    let resultat = [];

    // Pour chaque colonne

    for (let i = 0; i < original.length; i++) {

        let ligne = [];

        //Je prend chacune des valeurs, en commençant par la fin

        for (let j = original[i].length-1; j >= 0; j--) {
            ligne.push(original[j][i]);
        }
        // Puis j'ajoute cette ligne dans le résultat final

        resultat.push(ligne);
    }

    return resultat;
}

console.log(tab1);

console.log(convertir_tableau(tab1));