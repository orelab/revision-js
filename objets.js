import Animal from './lib/Animal.js';
import Oiseau from './lib/Oiseau.js';
import Zoo from './lib/Zoo.js';

const morice = new Animal('Maurice', 12, true);
const marcel = new Animal('Marcel', 12, true);
const mini = new Animal('Mini', 0.1, true);
const poulette = new Oiseau('Mack3', 1, true);

morice.manger(5);
morice.manger(3);



const zoo = new Zoo('Le zoo d\'Orel !');

zoo.add(morice);
zoo.add(marcel);
zoo.add(mini);
zoo.add(poulette);

zoo.afficher();

